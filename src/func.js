const getSum = (str1, str2) => {
	let a = Number(str1);
	let b = Number(str2);
	if (isNaN(a) || isNaN(b)) {
		return false;
	}
	if (a == 0 && b == 0) {
		return false;
	}

	console.log(a + " " + b);
	let c = a + b;
	return String(c);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
	let counter = { Post: 0, comments: 0 };
	for (let i = 0; i < listOfPosts.length; i++) {
		if (listOfPosts[i].author == authorName) {
			counter.Post++;
			if (listOfPosts[i].hasOwnProperty('comments')) {
				counter.comments = counter.comments + listOfPosts[i].comments.length;
			}
		}
	}
	return `Post:${counter.Post},comments:${counter.comments}`;
}

const tickets = (people) => {
	let values = new Array();
	let element = { nominal: 25, count: 0 };
	values.push(element);
	element = { nominal: 50, count: 0 };
	values.push(element);
	element = { nominal: 100, count: 0 };
	values.push(element);

	let can = true;
	for (let i = 0; i < people.length; i++) {
		if (people[i] == 25) {
			values[0].count++;
		}

		if (people[i] == 50) {
			if (values[0].count == 0) {
				return "NO";
			}
			values[1].count++;
			values[0].count--;
		}
		if (people[i] == 100) {
			if (values[0].count == 0) {
				return "NO";
			}
			if (values[1].count > 0 && values[0].count > 0) {
				values[0].count--;
				values[1].count--;
				values[2].count++;
			}
			else if (values[1].count == 0 && values[0].count >= 3) {
				values[0].count -= 3;
				values[2].count++;
			}
		}
	}
	return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
